"use strict";

var gulp = require("gulp"),
	browserSync = require("browser-sync").create(),
	sass = require("gulp-sass"),
	reload = browserSync.reload(),
	del = require("del"),
	imagemin = require("gulp-imagemin"),
	uglify = require("gulp-uglify"),
	usemin = require("gulp-usemin"),
	rev = require("gulp-rev"),
	cleanCss = require("gulp-clean-css"),
	flatmap = require("gulp-flatmap"),
	htmlmin = require("gulp-htmlmin");

gulp.task("sass", function () {
	return gulp.src("./css/*.scss").pipe(sass.sync().on("error", sass.logError)).pipe(gulp.dest("./css"));
});

gulp.task("sass:watch", function (done) {
	gulp.watch("./css/*.scss", gulp.series(["sass"]));
});

gulp.task("browser-sync", function () {
	browserSync.init({
		server: {
			baseDir: "./",
		},
	});
	gulp.watch("./css/*.css").on("change", reload);
	gulp.watch("./js/*.js").on("change", reload);
	gulp.watch("./*.html").on("change", reload);
});

gulp.task("default", gulp.parallel(["browser-sync", "sass:watch"]), function () {
	gulp.start("sass:watch");
});

gulp.task("clean", function () {
	return del("dist");
});
gulp.task("imagemin", function () {
	return gulp
		.src("./images/*.+(png|jpg|gif|svg)")
		.pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
		.pipe(gulp.dest("dist/images"));
});

gulp.task("fontAwesome", function () {
	return gulp.src("./node_modules/font-awesome/fonts/*").pipe(gulp.dest("dist/fonts"));
});
gulp.task("fontIconic", function () {
	return gulp.src("./node_modules/open-iconic/font/fonts/*").pipe(gulp.dest("dist/fonts"));
});

gulp.task("usemin", function () {
	return gulp
		.src("./*.html")
		.pipe(
			flatmap(function (stream, file) {
				return stream.pipe(
					usemin({
						css: [rev()],
						html: [
							function () {
								return htmlmin({ collapseWhitespace: true });
							},
						],
						js: [uglify(), rev()],
						inlinejs: [uglify()],
						inlinecss: [cleanCss(), "concat"],
					})
				);
			})
		)
		.pipe(gulp.dest("dist/"));
	//flat map allows us to process in paralel
});
gulp.task("build", gulp.series(["clean", "imagemin", "usemin", "fontAwesome", "fontIconic"]), function () {
	console.log("Building files");
});
